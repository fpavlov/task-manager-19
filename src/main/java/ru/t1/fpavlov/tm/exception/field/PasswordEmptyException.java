package ru.t1.fpavlov.tm.exception.field;

/**
 * Created by fpavlov on 20.12.2021.
 */
public final class PasswordEmptyException extends AbstractFieldException {

    public PasswordEmptyException() {
        super("Error! Password is empty");
    }

}
