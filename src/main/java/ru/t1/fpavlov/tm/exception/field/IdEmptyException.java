package ru.t1.fpavlov.tm.exception.field;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error! Id is empty");
    }

}
