package ru.t1.fpavlov.tm.command.entity.task;

import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskListCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "List Task";

    public static final String NAME = "task-list";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Sort sort = this.askEntitySort();
        final List<Task> tasks = this.getTaskService().findAll(sort);
        this.renderEntities(tasks, "Tasks:");
    }

}
