package ru.t1.fpavlov.tm.command.system;

import ru.t1.fpavlov.tm.command.AbstractCommand;

/**
 * Created by fpavlov on 09.12.2021.
 */
public final class ApplicationQuiteCommand extends AbstractCommand {

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Quite";

    public static final String NAME = "exit";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
