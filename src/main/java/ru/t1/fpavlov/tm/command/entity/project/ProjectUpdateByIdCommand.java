package ru.t1.fpavlov.tm.command.entity.project;

import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Update project by id";

    public static final String NAME = "project-update-by-id";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Project entity = this.findById();
        final String name = this.askEntityName();
        final String description = this.askEntityDescription();
        this.getProjectService().update(entity, name, description);
    }

}
