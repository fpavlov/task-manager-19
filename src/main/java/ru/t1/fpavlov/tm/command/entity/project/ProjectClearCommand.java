package ru.t1.fpavlov.tm.command.entity.project;

import ru.t1.fpavlov.tm.model.Project;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Clear project repository";

    public static final String NAME = "project-clear";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final List<Project> projects = this.getProjectService().findAll();
        for (final Project project : projects) {
            this.getProjectTaskService().removeProject(project);
        }
    }

}
