package ru.t1.fpavlov.tm.command.system;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Display info about author";

    public static final String NAME = "about";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        System.out.println("\tPavlov Philipp");
        System.out.println("\tfpavlov@t1-consulting.ru");
    }

}
