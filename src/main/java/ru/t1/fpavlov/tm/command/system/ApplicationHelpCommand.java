package ru.t1.fpavlov.tm.command.system;


import ru.t1.fpavlov.tm.api.model.ICommand;
import ru.t1.fpavlov.tm.command.AbstractCommand;

import java.util.Collection;

/**
 * Created by fpavlov on 07.12.2021.
 */
public final class ApplicationHelpCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-h";

    public static final String DESCRIPTION = "Display available commands";

    public static final String NAME = "help";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = this.getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            System.out.println(command);
        }
    }

}
