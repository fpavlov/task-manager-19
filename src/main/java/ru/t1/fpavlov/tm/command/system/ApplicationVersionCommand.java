package ru.t1.fpavlov.tm.command.system;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-v";

    public static final String DESCRIPTION = "Display current application version";

    public static final String NAME = "version";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        System.out.println("\t1.19.0");
    }

}
