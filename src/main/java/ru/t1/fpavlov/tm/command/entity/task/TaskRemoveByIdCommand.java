package ru.t1.fpavlov.tm.command.entity.task;

import ru.t1.fpavlov.tm.model.Task;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Remove Task by id";

    public static final String NAME = "task-remove-by-id";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Task entity = this.findById();
        this.getTaskService().remove(entity);
    }

}
