package ru.t1.fpavlov.tm.command.entity;

import ru.t1.fpavlov.tm.api.service.IProjectTaskService;
import ru.t1.fpavlov.tm.command.AbstractCommand;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.Arrays;

/**
 * Created by fpavlov on 08.12.2021.
 */
public abstract class AbstractEntityCommand extends AbstractCommand {

    protected static final String ARGUMENT = null;

    protected final IProjectTaskService getProjectTaskService() {
        return this.getServiceLocator().getProjectTaskService();
    }

    protected final Status askEntityStatus() {
        System.out.println("Available statuses:");
        System.out.println(Arrays.toString(Status.displayValues()));
        final String statusValue = TerminalUtil.nextLine();
        return Status.toStatus(statusValue);
    }

    protected final String askEntityName() {
        System.out.println("Enter name");
        return TerminalUtil.nextLine();
    }

    protected final String askEntityDescription() {
        System.out.println("Enter description");
        return TerminalUtil.nextLine();
    }

    protected final Sort askEntitySort() {
        System.out.println("Available sort:");
        System.out.println(Arrays.toString(Sort.displayValues()));
        final String sortType = TerminalUtil.nextLine();
        return Sort.toSort(sortType);
    }

}
