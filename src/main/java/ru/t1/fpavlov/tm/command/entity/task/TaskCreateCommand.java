package ru.t1.fpavlov.tm.command.entity.task;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskCreateCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Create new Task";

    public static final String NAME = "task-create";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final String name = this.askEntityName();
        final String description = this.askEntityDescription();
        this.getTaskService().create(name, description);
    }

}
