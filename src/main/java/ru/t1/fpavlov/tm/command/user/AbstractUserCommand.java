package ru.t1.fpavlov.tm.command.user;

import ru.t1.fpavlov.tm.api.service.IAuthService;
import ru.t1.fpavlov.tm.command.AbstractCommand;
import ru.t1.fpavlov.tm.model.User;
import ru.t1.fpavlov.tm.util.TerminalUtil;

/**
 * Created by fpavlov on 21.12.2021.
 */
public abstract class AbstractUserCommand extends AbstractCommand {

    protected static final String ARGUMENT = null;

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    public IAuthService getAuthService() {
        return this.getServiceLocator().getAuthService();
    }

    public String input(final String displayText) {
        if (displayText != null || !displayText.isEmpty()) System.out.println(displayText);
        return TerminalUtil.nextLine();
    }

    public void renderUser(final User user) {
        if (user != null) System.out.println(user);
    }

}
