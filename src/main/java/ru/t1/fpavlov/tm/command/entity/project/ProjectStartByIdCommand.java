package ru.t1.fpavlov.tm.command.entity.project;

import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Start project by id";

    public static final String NAME = "project-start-by-id";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Project entity = this.findById();
        this.getProjectService().changeStatus(entity, Status.IN_PROGRESS);
    }

}
