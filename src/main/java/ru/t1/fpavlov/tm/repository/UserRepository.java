package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.IUserRepository;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;
import ru.t1.fpavlov.tm.util.HashUtil;

/**
 * Created by fpavlov on 20.12.2021.
 */
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) {
        final String passwordHash = HashUtil.salt(password);
        return this.add(new User(login, passwordHash));
    }

    @Override
    public User create(final String login, final String password, final String email) {
        final String passwordHash = HashUtil.salt(password);
        return this.add(new User(login, passwordHash, email));
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        final String passwordHash = HashUtil.salt(password);
        return this.add(new User(login, passwordHash, role));
    }

    @Override
    public User findByLogin(final String login) {
        return this.findByTextField("login", login);
    }

    @Override
    public User findByEmail(final String email) {
        return this.findByTextField("email", email);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return this.findByTextField("login", login) != null;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return this.findByTextField("email", email) != null;
    }

}
