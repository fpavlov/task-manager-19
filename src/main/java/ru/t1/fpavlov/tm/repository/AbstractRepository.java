package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.IRepository;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.AbstractModel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 21.12.2021.
 */
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected List<M> items = new ArrayList<>();

    @Override
    public void clear() {
        this.items.clear();
    }

    @Override
    public int getSize() {
        return this.items.size();
    }

    @Override
    public List<M> findAll() {
        return new ArrayList<>(this.items);
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        final List<M> items = this.findAll();
        items.sort(comparator);
        return items;
    }

    @Override
    public List<M> findAll(Sort sort) {
        if (sort == null) return this.findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public M add(final M item) {
        this.items.add(item);
        return item;
    }

    private String calculateGetterName(final String fieldName) {
        String getterName = "get";
        final String nameParts[] = fieldName.toLowerCase().split(" ");
        for (final String part : nameParts) {
            getterName += Character.toUpperCase(part.charAt(0)) + part.substring(1);
        }

        return getterName;
    }

    protected M findByTextField(String fieldName, final String searchValue) {
        if (fieldName == null || fieldName.isEmpty()) return null;
        if (searchValue == null || searchValue.isEmpty()) return null;

        final Class<M> clazz =
                (Class<M>) ((ParameterizedType) getClass().getGenericSuperclass())
                        .getActualTypeArguments()[0];

        final String methodName = this.calculateGetterName(fieldName);
        M foundItem = null;

        try {
            final Method method = clazz.getMethod(methodName);
            for (final M item : this.items) {
                if (searchValue.equals(method.invoke(item))) {
                    foundItem = item;
                    break;
                }
            }
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            foundItem = null;
        }

        return foundItem;
    }

    @Override
    public M findById(final String id) {
        return this.findByTextField("id", id);
    }

    @Override
    public M findByIndex(final Integer index) {
        return this.items.get(index);
    }

    @Override
    public M remove(final M item) {
        this.items.remove(item);
        return item;
    }

    @Override
    public M removeById(final String id) {
        final M item = this.findById(id);
        if (item == null) return null;
        return this.remove(item);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M item = this.findByIndex(index);
        if (item == null) return null;
        return this.remove(item);
    }

    @Override
    public boolean isIdExist(final String id) {
        return this.findById(id) != null;
    }

}
