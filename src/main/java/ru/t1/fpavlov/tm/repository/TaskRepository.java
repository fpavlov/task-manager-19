package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.exception.field.DescriptionEmptyException;
import ru.t1.fpavlov.tm.exception.field.NameEmptyException;
import ru.t1.fpavlov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return this.add(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return this.add(new Task(name, description));
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : this.items) {
            if (projectId.equals(task.getProjectId())) result.add(task);
        }
        return result;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId, final Comparator comparator) {
        final List<Task> result = this.findAllByProjectId(projectId);
        if (comparator != null) result.sort(comparator);
        return result;
    }

}
