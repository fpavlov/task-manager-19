package ru.t1.fpavlov.tm.component;


import ru.t1.fpavlov.tm.api.repository.ICommandRepository;
import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.repository.IUserRepository;
import ru.t1.fpavlov.tm.api.service.*;
import ru.t1.fpavlov.tm.command.AbstractCommand;
import ru.t1.fpavlov.tm.command.entity.project.*;
import ru.t1.fpavlov.tm.command.entity.task.*;
import ru.t1.fpavlov.tm.command.system.*;
import ru.t1.fpavlov.tm.command.user.*;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.exception.system.ArgumentIncorrectException;
import ru.t1.fpavlov.tm.exception.system.CommandIncorrectException;
import ru.t1.fpavlov.tm.repository.CommandRepository;
import ru.t1.fpavlov.tm.repository.ProjectRepository;
import ru.t1.fpavlov.tm.repository.TaskRepository;
import ru.t1.fpavlov.tm.repository.UserRepository;
import ru.t1.fpavlov.tm.service.*;
import ru.t1.fpavlov.tm.util.TerminalUtil;

/*
 * Created by fpavlov on 06.10.2021.
 */
public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(
            projectRepository,
            taskRepository
    );

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        this.registry(new ApplicationAboutCommand());
        this.registry(new ApplicationHelpCommand());
        this.registry(new ApplicationSystemInfoCommand());
        this.registry(new ApplicationVersionCommand());
        this.registry(new ApplicationQuiteCommand());
        this.registry(new ProjectChangeStatusByIdCommand());
        this.registry(new ProjectChangeStatusByIndexCommand());
        this.registry(new ProjectClearCommand());
        this.registry(new ProjectCompleteByIdCommand());
        this.registry(new ProjectCompleteByIndexCommand());
        this.registry(new ProjectCreateCommand());
        this.registry(new ProjectFindByIdCommand());
        this.registry(new ProjectFindByIndexCommand());
        this.registry(new ProjectListCommand());
        this.registry(new ProjectRemoveByIdCommand());
        this.registry(new ProjectRemoveByIndexCommand());
        this.registry(new ProjectStartByIdCommand());
        this.registry(new ProjectStartByIndexCommand());
        this.registry(new ProjectUpdateByIdCommand());
        this.registry(new ProjectUpdateByIndexCommand());
        this.registry(new TaskBindToProjectCommand());
        this.registry(new TaskChangeStatusByIdCommand());
        this.registry(new TaskChangeStatusByIndexCommand());
        this.registry(new TaskClearCommand());
        this.registry(new TaskCompleteByIdCommand());
        this.registry(new TaskCompleteByIndexCommand());
        this.registry(new TaskCreateCommand());
        this.registry(new TaskFindByIdCommand());
        this.registry(new TaskFindByIndexCommand());
        this.registry(new TaskListByProjectCommand());
        this.registry(new TaskListCommand());
        this.registry(new TaskRemoveByIdCommand());
        this.registry(new TaskRemoveByIndexCommand());
        this.registry(new TaskStartByIdCommand());
        this.registry(new TaskStartByIndexCommand());
        this.registry(new TaskUnbindToProjectCommand());
        this.registry(new TaskUpdateByIdCommand());
        this.registry(new TaskUpdateByIndexCommand());
        this.registry(new UserLoginCommand());
        this.registry(new UserLogoutCommand());
        this.registry(new UserRegistryCommand());
        this.registry(new UserChangePasswordCommand());
        this.registry(new UserUpdateProfileCommand());
        this.registry(new UserShowProfileCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return this.commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return this.loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return this.projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return this.projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return this.taskService;
    }

    @Override
    public IUserService getUserService() {
        return this.userService;
    }

    @Override
    public IAuthService getAuthService() {
        return this.authService;
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        this.commandService.add(command);
    }

    private void listenerCommand(final String commandName) {
        final AbstractCommand command = this.getCommandService().getCommandByName(commandName);
        if (command == null) throw new CommandIncorrectException();
        command.execute();
    }

    private void listenerArgument(final String argument) {
        final AbstractCommand command = this.getCommandService().getCommandByArgument(argument);
        if (command == null) throw new ArgumentIncorrectException();
        this.quite();
    }

    private void initLogger() {
        this.loggerService.info("**WELCOME TO TASK MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("TASK-MANAGER IS SHUTTING DOWN");
            }
        });
    }

    public void run(final String[] args) {
        if (areArgumentsAvailable(args)) {
            try {
                listenerArgument(args[0]);
                this.loggerService.command(args[0]);
                return;
            } catch (Exception e) {
                this.loggerService.error(e);
                return;
            }
        }

        interactiveCommandProcessing();
    }

    private void interactiveCommandProcessing() {
        this.InitDemoData();
        this.initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("-- Please enter a command --");
                final String param = TerminalUtil.nextLine();
                listenerCommand(param);
                System.out.println("Ok");
                this.loggerService.command(param);
            } catch (Exception e) {
                this.loggerService.error(e);
            }
        }
    }

    private static boolean areArgumentsAvailable(final String[] args) {
        return (args != null && args.length > 0);
    }

    private static void quite() {
        System.exit(0);
    }

    private void InitDemoData() {
        this.projectService.create("Test project 4", "Test project 4");
        this.projectService.create("Test project 1", "Test project 1");
        this.projectService.create("Test project 3", "Test project 3");
        this.projectService.create("Test project 2", "Test project 2");
        this.projectService.create("Test project 5", "Test project 5");

        this.taskService.create("Test task 10", "Test task 10");
        this.taskService.create("Test task 9", "Test task 9");
        this.taskService.create("Test task 3", "Test task 3");
        this.taskService.create("Test task 13", "Test task 13");
        this.taskService.create("Test task 7", "Test task 7");
        this.taskService.create("Test task 5", "Test task 5");
        this.taskService.create("Test task 14", "Test task 14");
        this.taskService.create("Test task 2", "Test task 2");
        this.taskService.create("Test task 6", "Test task 6");
        this.taskService.create("Test task 8", "Test task 8");
        this.taskService.create("Test task 4", "Test task 4");
        this.taskService.create("Test task 12", "Test task 12");
        this.taskService.create("Test task 1", "Test task 1");
        this.taskService.create("Test task 15", "Test task 15");
        this.taskService.create("Test task 11", "Test task 11");

        this.userService.create("test1", "123", "user1@user.com");
        this.userService.create("test2", "123", "user2@user.com");
        this.userService.create("admin", "321", Role.ADMIB);
    }
}
