package ru.t1.fpavlov.tm.model;

import ru.t1.fpavlov.tm.api.model.IWBS;
import ru.t1.fpavlov.tm.enumerated.Status;

import java.util.Date;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class Project extends AbstractModel implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public Status getStatus() {
        return this.status;
    }

    @Override
    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public Date getCreated() {
        return this.created;
    }

    @Override
    public String toString() {
        return "".format(" |%40s |%10s |%20s |%20s |%20s |",
                this.getId(),
                this.created,
                this.name,
                this.description,
                Status.toName(this.status));
    }

}
