package ru.t1.fpavlov.tm.model;

import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 19.12.2021.
 */
public final class User extends AbstractModel {

    private String login;

    private String email;

    private String passwordHash;

    private String firstName;

    private String lastName;

    private String middleName;

    private Role role = Role.USER;

    public User() {
    }

    public User(final String login, final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(final String login, final String pawordHash, final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public User(final String login, final String passwordHash, final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return this.passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "".format(" |%40s |%10s |%10s |%10s |%10s |%10s |%20s |",
                this.getId(),
                this.login,
                this.firstName,
                this.lastName,
                this.middleName,
                this.email,
                this.role.getDisplayName());
    }

}
