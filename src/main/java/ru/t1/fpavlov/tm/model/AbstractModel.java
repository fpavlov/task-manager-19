package ru.t1.fpavlov.tm.model;

import java.util.UUID;

/**
 * Created by fpavlov on 21.12.2021.
 */
public abstract class AbstractModel {

    private String id = UUID.randomUUID().toString();

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public static AbstractModel create() {
        return null;
    }

}
