package ru.t1.fpavlov.tm.enumerated;

/**
 * Created by fpavlov on 19.12.2021.
 */
public enum Role {

    USER("Common user"),
    ADMIB("System admin");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return this.displayName;
    }

}
