package ru.t1.fpavlov.tm.api.repository;

import ru.t1.fpavlov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface ITaskRepository extends IRepository<Task> {

    Task create(final String name);

    Task create(final String name, final String description);

    List<Task> findAllByProjectId(final String projectId);

    List<Task> findAllByProjectId(final String projectId, final Comparator comparator);

}
