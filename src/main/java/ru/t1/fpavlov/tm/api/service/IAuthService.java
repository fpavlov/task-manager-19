package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 21.12.2021.
 */
public interface IAuthService {

    User registry(final String login, final String password, final String email);

    void login(final String login, final String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}
