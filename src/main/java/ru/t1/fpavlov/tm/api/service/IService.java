package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.api.repository.IRepository;
import ru.t1.fpavlov.tm.model.AbstractModel;

/**
 * Created by fpavlov on 23.12.2021.
 */
public interface IService<M extends AbstractModel> extends IRepository<M> {

}
