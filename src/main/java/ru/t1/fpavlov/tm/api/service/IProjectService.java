package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 10.10.2021.
 */
public interface IProjectService extends IService<Project> {

    Project create(String name);

    Project create(String name, String description);

    Project changeStatus(final Project project, final Status status);

    Project changeStatusById(final String id, final Status status);

    Project changeStatusByIndex(final Integer index, final Status status);

    Project update(final Project project, final String name, final String description);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

}
