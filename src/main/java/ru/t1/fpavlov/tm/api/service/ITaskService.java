package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface ITaskService extends IService<Task> {

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAllByProjectId(final String projectId);

    List<Task> findAllByProjectId(final String projectId, final Sort sort);

    Task update(final Task task, final String name, final String description);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final Integer index, final String name, final String description);

    Task changeStatus(final Task task, final Status status);

    Task changeStatusById(final String id, final Status status);

    Task changeStatusByIndex(final Integer index, final Status status);

}
