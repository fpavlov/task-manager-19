package ru.t1.fpavlov.tm.api.repository;

import ru.t1.fpavlov.tm.model.Project;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface IProjectRepository extends IRepository<Project> {

    Project create(final String name);

    Project create(final String name, final String description);

}
